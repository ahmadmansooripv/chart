#include "csvdatareader.h"

CSVDataReader::CSVDataReader(QObject *parent) : QObject(parent)
{

}

void CSVDataReader::handleCvs(QString path,bool readAsRow)
{
    if (readAsRow){
        QFile csv_data(path);
        if (!csv_data.open(QIODevice::ReadOnly)){
            qDebug()<<"cannot open the csv file";
            return;
        }
        QString firstline = csv_data.readLine();
        for (auto col_name:firstline.split(",")){
            columns.append(col_name.split(" ")[0]);
        }
        rows.append(columns);
        //    qDebug()<<columns;
        while(!csv_data.atEnd()){
            QString row = csv_data.readLine();
            if (row.size()-1 == columns.size()){
                break;
            }

            QStringList current;
            for(auto value:row.split(",")){
                if (value != "..\r\n")
                    current.append(value);
            }
            rows.append(current);
            qDebug()<<current;

        }
        emit sendToChartModeler(columns,rows);

    }
    else{
        QFile csv_data(path);
        if (!csv_data.open(QIODevice::ReadOnly)){
            qDebug()<<"cannot open the csv file";
            return;
        }

        while(!csv_data.atEnd()){


        }

    }
}
