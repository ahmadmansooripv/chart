#include <QGuiApplication>
#include<QApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include<QDebug>
#include<QQmlContext>

#include "csvdatareader.h"
#include "lineseries.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);
    QQuickStyle::setStyle("Material");
    CSVDataReader myfile;

    lineSeries mychart;

    QObject::connect(&myfile,&CSVDataReader::sendToChartModeler,&mychart,&lineSeries::getDataAndConvert);

//    myfile.handleCvs("/BData.csv");
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("lineSeries",&mychart);
    engine.rootContext()->setContextProperty("fileloader",&myfile);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
