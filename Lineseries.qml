import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls 2.5
import QtCharts 2.3
import QtQuick.Controls.Material 2.3
import QtQuick.Dialogs 1.2

Rectangle{
//    anchors.fill: parent
    visible: false
    id:secondrect
    property bool readAsRows: true
    onReadAsRowsChanged: {
        console.log(readAsRows)
    }

    Connections{
        target: lineSeries
        onPointsChanged:{

            axisX.max = lineSeries.xAxis[1];
            axisX.min = lineSeries.xAxis[0];
            axisY.max = lineSeries.yAxis[1]+10;
            axisY.min = lineSeries.yAxis[0];
            planchart.title = lineSeries.chartLabel;

            for(var i in lineSeries.points){

                console.log(lineSeries.points[i].x ,"and",lineSeries.points[i].y)
                mainchart.append(lineSeries.points[i].x,lineSeries.points[i].y)

            }
        }
    }


    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: shortcuts.home
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrls)
//            Qt.quit()
            var path = fileDialog.fileUrl.toString()
            path = path.replace(/^(file:\/{3})/,"")
            console.log(path)
            fileloader.handleCvs(path,readAsRows)
        }
        onRejected: {
            console.log("Canceled")
//            Qt.quit()
        }
//        Component.onCompleted: visible = true
    }



    Settingpopup{
        id: popup

        width: parent.width /2
        height: parent.height /2
        anchors.centerIn: parent
    }



    TabBar{
        Material.accent: Material.Blue
        id:tabbar
        anchors.top: parent.top
        TabButton{
            width: height
            Label{
                anchors.centerIn: parent

                font{
                    family: awesome
                    bold:true
                    pixelSize: 20
                }

                text:"\uf359"

            }
            onClicked: {

                mainchart.clear()
                screenstack.pop()

            }
        }
        TabButton{
            width: height
            Label{
                anchors.centerIn: parent
                font {
                    family: awesomes
                    pixelSize: 20

                }
                text:"\uf085"
            }
            onClicked: {
                popup.open()
            }

        }
        TabButton{
            width: height
            Label{
                anchors.centerIn: parent

                font{
                    family: awesome
                    bold:true
                    pixelSize: 20
                }

                text:"\uf574"

            }
            onClicked: {
               fileDialog.open()
            }
        }
    }


    color: "transparent"
    ChartView{
        id:planchart
//        anchors.fill: parent
        width:parent.width
        height: width >400 ? parent.height : width
        anchors{
            top: tabbar.bottom
            bottom: parent.bottom
            margins: 20
        }
        titleFont: charttext

        antialiasing: true
//        backgroundColor: "transparent"
//        anchors.margins: 40
//        anchors.top: tabbar.bottom
        theme: ChartView.ChartThemeBlueNcs
        animationOptions: ChartView.NoAnimation
        animationDuration: 2000
        backgroundRoundness: 15
        onSeriesAdded: console.log("added")

        ValueAxis {
               id: axisX
               min: 1961
               max: 2020
               tickCount: 1
               tickInterval: 1
           }

        ValueAxis {
               id: axisY
               min: 0
               max:30
               tickCount:11
               tickInterval: 1
//               minorTickCount: 10
           }
        LineSeries {
            //                        style:Qt.DashLine
            id:mainchart
            name: ""
            axisX: axisX
            axisY: axisY

        }
    }




    Component.onCompleted: {

        console.log("badihiyat")
//        mainchart.append(1,1)
        for(var i in lineSeries.points){

            console.log(lineSeries.points[i].x ,"and",lineSeries.points[i].y)
            mainchart.append(lineSeries.points[i].x,lineSeries.points[i].y)

        }
    }
}
