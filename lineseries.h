#ifndef LINESERIES_H
#define LINESERIES_H

#include <QObject>
#include<QPointF>
#include<QVector>
#include<QDebug>
#include<QString>

class lineSeries : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVector<QPointF> points READ getPoints WRITE setPoints NOTIFY pointsChanged)
    Q_PROPERTY(QString chartLabel READ getChartLabel WRITE setChartLabel NOTIFY chartLabelChanged)
    Q_PROPERTY(QVector<qreal> xAxis READ getXAxis WRITE setXAxis NOTIFY xAxisChanged)
    Q_PROPERTY(QVector<qreal> yAxis READ getYAxis WRITE setYAxis NOTIFY yAxisChanged)

public:
    explicit lineSeries(QObject *parent = nullptr);
    Q_INVOKABLE QVector<QPointF> getPoints() const;
    Q_INVOKABLE QString getChartLabel() const;
    Q_INVOKABLE QVector<qreal> getXAxis() const;
    Q_INVOKABLE QVector<qreal> getYAxis() const;



public slots:
    void getDataAndConvert(QStringList,QList<QStringList>);
private:
    void setPoints(QVector<QPointF>& value);
    void setChartLabel(QString& value);
    void setXAxis(QVector<qreal>& value);
    void setYAxis(QVector<qreal>& value);

    QVector<QPointF> m_points;
    QString m_chartLabel;
    QVector<qreal> m_xAxis;
    QVector<qreal> m_yAxis;
signals:
    void pointsChanged();
    void chartLabelChanged();
    void xAxisChanged();
    void yAxisChanged();

};

#endif // LINESERIES_H
