import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls 2.12
import QtCharts 2.3
import QtQuick.Extras 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.3

ApplicationWindow {
    id:mainwindow
    visible: true
    width: 1280
    height: 720
    title: qsTr("QChart")
    Material.theme: Material.Dark
    minimumWidth: 350


    FontLoader{
        source: "/fonts/Font Awesome 5 Free-Regular-400.otf"
        id:awesome
    }

    FontLoader{
        source: "/fonts/C:/Users/sub/Downloads/IMFeGPit29P.ttf"
        id:charttext
    }

    FontLoader{
        source: "/fonts/Font Awesome 5 Free-Solid-900.otf"
        id:awesomes
    }

    StackView{
        id:screenstack
        anchors.fill: parent
        initialItem:df
        Lineseries{id:ls}
        //        visible: false
    }

//    Rectangle{

        //        anchors.fill: parent
        Flickable {
            id:df

            anchors.fill: parent
            contentHeight: gridlay.height
            //                contentWidth: gridlay.width

            ScrollBar.vertical: ScrollBar {
                visible: false    // hides scrollbar
            }
            GridLayout{
                id:gridlay
                columns:mainwindow.width>mainwindow.height ? 3 :1
                anchors.horizontalCenter: parent.horizontalCenter

                Item {
                    id: piechart
                    Layout.maximumWidth: 300
                    Layout.minimumWidth: 100
                    implicitWidth: 300

                    Layout.maximumHeight: 300
                    Layout.minimumHeight: 100
                    implicitHeight: 300
                    Button{
                        anchors.fill: parent


                        ChartView{
                            anchors.bottomMargin: 10
                            anchors.topMargin: 10
                            //                    backgroundColor: "transparent"
                            backgroundRoundness:0
                            dropShadowEnabled:true
                            anchors.fill: parent
                            antialiasing: true
                            Material.background: "transparent"
                            //                    theme: ChartView.ChartThemeLight
                            legend.alignment: Qt.AlignBottom
                            PieSeries {
                                id: pieSeries
                                PieSlice { label: ""; value: 94.9-39 }
                                PieSlice { label: ""; value: 15.1 }
                                PieSlice{label:" ";value: 29}
                            }
                            Component.onCompleted: {
                                pieSeries.find(" ").exploded = true;
                            }

                        }

                    }


                }
                Item {
                    id: piechart1
                    Layout.maximumWidth: 300
                    Layout.minimumWidth: 100
                    implicitWidth: 300

                    Layout.maximumHeight: 300
                    Layout.minimumHeight: 100
                    implicitHeight: 300
                    Button{
                        anchors.fill: parent
                        onClicked: {
                            console.log(screenstack.depth)
                            screenstack.push(ls)

                        }

                        ChartView{
                            MouseArea{
                                anchors.fill: parent
                                onClicked:
                                {
                                   screenstack.push(ls)
                                }
                            }

                            anchors.bottomMargin: 10
                            anchors.topMargin: 10
                            //                    backgroundColor: "transparent"
                            backgroundRoundness:0
                            anchors.fill: parent
                            antialiasing: true
                            Material.background: "transparent"
                            theme: ChartView.ChartThemeBlueNcs
                            legend.alignment: Qt.AlignBottom

                            LineSeries {
                                //                        style:Qt.DashLine
                                name: ""
                                XYPoint { x: 0; y: 0 }
                                XYPoint { x: 1.1; y: 2.1 }
                                XYPoint { x: 1.9; y: 3.3 }
                                XYPoint { x: 2.1; y: 2.1 }
                                //                        XYPoint { x: 2.9; y: 4.9 }
                                //                        XYPoint { x: 3.4; y: 3.0 }
                                //                        XYPoint { x: 4.1; y: 3.3 }
                            }

                        }

                    }


                }
                Item {
                    id: piechart2
                    Layout.maximumWidth: 300
                    Layout.minimumWidth: 100
                    implicitWidth: 300

                    Layout.maximumHeight: 300
                    Layout.minimumHeight: 100
                    implicitHeight: 300

                    Button{
                        anchors.fill: parent
                        ChartView{
                            anchors.bottomMargin: 10
                            anchors.topMargin: 10

                            //                    backgroundColor: "transparent"
                            backgroundRoundness:0
                            dropShadowEnabled:true
                            anchors.fill: parent
                            antialiasing: true
                            Material.background: "transparent"
                            //                    theme: ChartView.ChartThemeBlueNcs
                            legend.alignment: Qt.AlignBottom
                            SplineSeries {

                                name: ""
                                XYPoint { x: 0; y: 0.0 }
                                XYPoint { x: 1.1; y: 2.8 }
                                XYPoint { x: 1.9; y: 2.4 }
                                XYPoint { x: 2.1; y: 2.1 }
                                XYPoint { x: 2.9; y: 2.6 }
                                XYPoint { x: 3.4; y: 2.3 }
                                XYPoint { x: 4.1; y: 3 }
                            }
                        }
                    }


                }
                Item {
                    id: piechart3
                    Layout.maximumWidth: 300
                    Layout.minimumWidth: 100
                    implicitWidth: 300

                    Layout.maximumHeight: 300
                    Layout.minimumHeight: 100
                    implicitHeight: 300


                    Button{
                        anchors.fill: parent
                        ChartView{
                            anchors.topMargin: 10
                            anchors.bottomMargin: 10
                            antialiasing: true
                            legend.alignment: Qt.AlignBottom

                            anchors.fill: parent
                            ValueAxis {
                                id: valueAxis
                                min: 2000
                                max: 2003
                                tickCount: 12
                                labelFormat: "%.0f"
                            }

                            AreaSeries {

                                name: "Russian"
                                axisX: valueAxis
                                upperSeries: LineSeries {
                                    XYPoint { x: 2000; y: 1 }
                                    XYPoint { x: 2001; y: 6 }
                                    XYPoint { x: 2002; y: 1 }
                                    XYPoint { x: 2003; y: 9 }
                                    //                            XYPoint { x: 2004; y: 10 }
                                    //                            XYPoint { x: 2005; y: 0 }
                                    //                            XYPoint { x: 2006; y: 1 }
                                    //                            XYPoint { x: 2007; y: 1 }
                                    //                            XYPoint { x: 2008; y: 4 }
                                    //                            XYPoint { x: 2009; y: 3 }
                                    //                            XYPoint { x: 2010; y: 2 }
                                    //                            XYPoint { x: 2011; y: 1 }
                                }

                            }
                            AreaSeries {

                                name: "iran"
                                axisX: valueAxis
                                upperSeries: LineSeries {
                                    XYPoint { x: 2000; y: 1 }
                                    XYPoint { x: 2001; y: 5 }
                                    XYPoint { x: 2002; y: 1 }
                                    XYPoint { x: 2003; y: 1 }
                                    //                            XYPoint { x: 2004; y: 5 }
                                    //                            XYPoint { x: 2005; y: 0 }
                                    //                            XYPoint { x: 2006; y: 1 }
                                    //                            XYPoint { x: 2007; y: 1 }
                                    //                            XYPoint { x: 2008; y: 6 }
                                    //                            XYPoint { x: 2009; y: 3 }
                                    //                            XYPoint { x: 2010; y: 2 }
                                    //                            XYPoint { x: 2011; y: 1 }
                                }

                            }
                        }
                    }



                }
                Item {
                    id: piechart4
                    Layout.maximumWidth: 300
                    Layout.minimumWidth: 100
                    implicitWidth: 300

                    Layout.maximumHeight: 300
                    Layout.minimumHeight: 100
                    implicitHeight: 300
                    Button{
                        anchors.fill:parent
                        ChartView{
                            antialiasing: true
                            anchors.fill: parent
                            legend.alignment: Qt.AlignBottom
                            anchors.bottomMargin: 10
                            anchors.topMargin: 10
                            ScatterSeries {
                                id: scatter1
                                name: "Scatter1"
                                XYPoint { x: 1.5; y: 1.5 }
                                XYPoint { x: 1.5; y: 1.6 }
                                XYPoint { x: 1.57; y: 1.55 }
                                XYPoint { x: 1.8; y: 1.8 }
                                XYPoint { x: 1.9; y: 1.6 }
                                XYPoint { x: 2.1; y: 1.3 }
                                XYPoint { x: 2.5; y: 2.1 }
                            }
                            ScatterSeries {
                                id: scatter2
                                name: "Scatter1"
                                XYPoint { x: 1.6; y: 103 }
                                XYPoint { x: 1.8; y: 1.2 }
                                XYPoint { x: 1.67; y: 1.25 }
                                XYPoint { x: 1.8; y: 1.8 }
                                XYPoint { x: 1.9; y: 1.6 }
                                XYPoint { x: 2.1; y: 1.3 }
                                XYPoint { x: 2.5; y: 2.1 }
                            }
                        }
                    }



                }
                Item {
                    id: piechart5
                    Layout.maximumWidth: 300
                    Layout.minimumWidth: 100
                    implicitWidth: 300

                    Layout.maximumHeight: 300
                    Layout.minimumHeight: 100
                    implicitHeight: 300
                    Button{
                        anchors.fill:parent
                        ChartView{
                            anchors.fill: parent
                            antialiasing: true
                            anchors.topMargin: 10
                            anchors.bottomMargin: 10
                            BarSeries {
                                id: mySeries
                                axisX: BarCategoryAxis { categories: ["2007", "2008", "2009" ] }
                                BarSet { label: "Bob"; values: [2, 2, 3] }
                                BarSet { label: "Susan"; values: [5, 1, 2] }
                                BarSet { label: "James"; values: [3, 5, 8] }
                            }
                        }
                    }


                }
                Item {
                    id: piechart6
                    Layout.maximumWidth: 300
                    Layout.minimumWidth: 100
                    implicitWidth: 300

                    Layout.maximumHeight: 300
                    Layout.minimumHeight: 100
                    implicitHeight: 300
                    Button{
                        anchors.fill:parent
                        ChartView{
                            anchors.fill: parent
                            antialiasing: true
                            anchors.topMargin: 10
                            anchors.bottomMargin: 10
                            StackedBarSeries {
                                id: stackseries
                                axisX: BarCategoryAxis { categories: ["2007", "2008", "2009" ] }
                                BarSet { label: "Bob"; values: [2, 2, 3] }
                                BarSet { label: "Susan"; values: [5, 1, 2] }
                                BarSet { label: "James"; values: [3, 5, 8] }
                            }
                        }
                    }


                }
                Item {
                    id: piechart7
                    Layout.maximumWidth: 300
                    Layout.minimumWidth: 100
                    implicitWidth: 300

                    Layout.maximumHeight: 300
                    Layout.minimumHeight: 100
                    implicitHeight: 300
                    Button{
                        anchors.fill:parent
                        ChartView{
                            anchors.fill: parent
                            antialiasing: true
                            anchors.topMargin: 10
                            anchors.bottomMargin: 10
                            PercentBarSeries {
                                axisX: BarCategoryAxis { categories: ["2007", "2008", "2009" ] }
                                BarSet { label: "Bob"; values: [2, 2, 3] }
                                BarSet { label: "Susan"; values: [5, 1, 2] }
                                BarSet { label: "James"; values: [3, 5, 8] }
                            }
                        }
                    }


                }
                Item {
                    id: piechart8
                    Layout.maximumWidth: 300
                    Layout.minimumWidth: 100
                    implicitWidth: 300

                    Layout.maximumHeight: 300
                    Layout.minimumHeight: 100
                    implicitHeight: 300
                    Button{
                        anchors.fill:parent
                        ChartView{
                            anchors.fill: parent
                            antialiasing: true
                            anchors.topMargin: 10
                            anchors.bottomMargin: 10
                            HorizontalBarSeries {
                                axisY: BarCategoryAxis { categories: ["2007", "2008", "2009" ] }
                                BarSet { label: "Bob"; values: [2, 2, 3] }
                                BarSet { label: "Susan"; values: [5, 1, 2] }
                                BarSet { label: "James"; values: [3, 5, 8] }
                            }
                        }
                    }


                }
                Item {
                    id: piechart9
                    Layout.maximumWidth: 300
                    Layout.minimumWidth: 100
                    implicitWidth: 300

                    Layout.maximumHeight: 300
                    Layout.minimumHeight: 100
                    implicitHeight: 300
                    Button{
                        anchors.fill:parent
                        ChartView{
                            anchors.fill: parent
                            antialiasing: true
                            anchors.topMargin: 10
                            anchors.bottomMargin: 10
                            HorizontalStackedBarSeries {
                                axisY: BarCategoryAxis { categories: ["2007", "2008", "2009" ] }
                                BarSet { label: "Bob"; values: [2, 2, 3] }
                                BarSet { label: "Susan"; values: [5, 1, 2] }
                                BarSet { label: "James"; values: [3, 5, 8] }
                            }
                        }
                    }


                }
                Item {
                    id: piechart10
                    Layout.maximumWidth: 300
                    Layout.minimumWidth: 100
                    implicitWidth: 300

                    Layout.maximumHeight: 300
                    Layout.minimumHeight: 100
                    implicitHeight: 300
                    Button{
                        anchors.fill:parent
                        ChartView{
                            anchors.fill: parent
                            antialiasing: true
                            anchors.topMargin: 10
                            anchors.bottomMargin: 10
                            HorizontalPercentBarSeries {
                                axisY: BarCategoryAxis { categories: ["2007", "2008", "2009" ] }
                                BarSet { label: "Bob"; values: [2, 2, 3] }
                                BarSet { label: "Susan"; values: [5, 1, 2] }
                                BarSet { label: "James"; values: [3, 5, 8] }
                            }
                        }
                    }


                }
                Item {
                    id: piechart11
                    Layout.maximumWidth: 300
                    Layout.minimumWidth: 100
                    implicitWidth: 300

                    Layout.maximumHeight: 300
                    Layout.minimumHeight: 100
                    implicitHeight: 300
                    Button{
                        anchors.fill:parent
                        ChartView{
                            anchors.fill: parent
                            antialiasing: true
                            anchors.topMargin: 10
                            anchors.bottomMargin: 10
                            PieSeries {
                                id: pieOuter
                                size: 0.96
                                holeSize: 0.7
                                PieSlice { id: slice; label: "Alpha"; value: 19511; color: "#99CA53" }
                                PieSlice { label: "Epsilon"; value: 11105; color: "#209FDF" }
                                PieSlice { label: "Psi"; value: 9352; color: "#F6A625" }
                            }

                            PieSeries {
                                size: 0.7
                                id: pieInner
                                holeSize: 0.25

                                PieSlice { label: "Materials"; value: 10334; color: "#B9DB8A" }
                                PieSlice { label: "Employee"; value: 3066; color: "#DCEDC4" }
                                PieSlice { label: "Logistics"; value: 6111; color: "#F3F9EB" }

                                PieSlice { label: "Materials"; value: 7371; color: "#63BCE9" }
                                PieSlice { label: "Employee"; value: 2443; color: "#A6D9F2" }
                                PieSlice { label: "Logistics"; value: 1291; color: "#E9F5FC" }

                                PieSlice { label: "Materials"; value: 4022; color: "#F9C36C" }
                                PieSlice { label: "Employee"; value: 3998; color: "#FCE1B6" }
                                PieSlice { label: "Logistics"; value: 1332; color: "#FEF5E7" }
                            }
                        }

                        Component.onCompleted: {
                            // Set the common slice properties dynamically for convenience
                            for (var i = 0; i < pieOuter.count; i++) {
                                pieOuter.at(i).labelPosition = PieSlice.LabelOutside;
                                pieOuter.at(i).labelVisible = true;
                                pieOuter.at(i).borderWidth = 3;
                            }
                            for (var i = 0; i < pieInner.count; i++) {
                                pieInner.at(i).labelPosition = PieSlice.LabelInsideNormal;
                                pieInner.at(i).labelVisible = true;
                                pieInner.at(i).borderWidth = 2;
                            }
                        }
                    }


                }
            }
            //        }
        }
    }

//}
