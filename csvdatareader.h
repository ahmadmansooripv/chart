#ifndef CSVDATAREADER_H
#define CSVDATAREADER_H

#include <QObject>
#include<QFile>
#include<QDebug>
#include<QStringList>
#include<QList>
#include<QByteArray>


class CSVDataReader : public QObject
{
    Q_OBJECT
public:
    explicit CSVDataReader(QObject *parent = nullptr);
    QStringList columns;
    QList<QStringList> rows;
    Q_INVOKABLE void handleCvs(QString path,bool readAsRow);

private:


signals:
    void sendToChartModeler(QStringList,QList<QStringList>);

};

#endif // CSVDATAREADER_H
