#include "lineseries.h"

lineSeries::lineSeries(QObject *parent) : QObject(parent)
{

}

void lineSeries::getDataAndConvert(QStringList column, QList<QStringList> rows)
{
    Q_UNUSED(column)
    qreal max=rows[1][0].toFloat();
    qreal min=rows[1].last().toFloat();
    for (auto i :rows[1]){
        qreal current = i.toFloat();
        if(max<current){
            max = current;
        }
        if (min>current){
            min = current;
        }

    }
    QStringList columns = rows[0];
    m_chartLabel = columns.at(0);
    m_yAxis<<min<<max;
    m_xAxis<<columns.at(4).toFloat()<<columns.last().toFloat();


    for(int i=4;i<=rows[1].size()-1;i++){
        m_points.append(QPointF(columns.at(i).toFloat(),rows[1].at(i).toFloat()));
    }
    for(auto value:m_points)
    qDebug()<<value.x()<<"*****"<<value.y();
    emit pointsChanged();

}

QVector<QPointF> lineSeries::getPoints() const
{
    return m_points;

}

QString lineSeries::getChartLabel() const
{
    return m_chartLabel;
}

QVector<qreal> lineSeries::getXAxis() const
{
    return m_xAxis;
}

QVector<qreal> lineSeries::getYAxis() const
{
    return m_yAxis;
}

void lineSeries::setPoints(QVector<QPointF> &value)
{
    Q_UNUSED(value)
}

void lineSeries::setChartLabel(QString &value)
{

}

void lineSeries::setXAxis(QVector<qreal> &value)
{

}

void lineSeries::setYAxis(QVector<qreal> &value)
{

}
