import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Item {
    width: parent.width/2
    height: parent.height/2
    anchors.centerIn: parent


    function open(){
        settings.open()
    }

    Popup {
        Label{
            id:label
            anchors.top: parent
            anchors.left: parent.Left
            text:"settings"
            font {
                bold:true
            }
        }
        id:settings
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        modal: false
        focus: true
        closePolicy: Popup.CloseOnPressOutside | Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent


        RowLayout{
            anchors.top: label.bottom
            anchors.centerIn: parent
            Label{
                text:"read csv file as : "
            }

            RadioButton{
                text:qsTr("Rows")
                checked: true
                onCheckedChanged: {
                    readAsRows = checked
                }

            }
            RadioButton{
                text:qsTr("Columns")
            }
            Item{
             height: parent.height/2
            }

        }


       }
}
