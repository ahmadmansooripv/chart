import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls 2.5
import QtCharts 2.3

Rectangle{
    id:secondrect
    color: 'yellow'
    ChartView{
        anchors.fill: parent
        antialiasing: true
        LineSeries {
            //                        style:Qt.DashLine
            name: ""
            XYPoint { x: 0; y: 0 }
            XYPoint { x: 1.1; y: 2.1 }
            XYPoint { x: 1.9; y: 3.3 }
            XYPoint { x: 2.1; y: 2.1 }

        }

    }


    MouseArea{
        anchors.fill: parent
        onClicked: {
            stack.pop()
        }
    }
}
